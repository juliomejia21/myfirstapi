<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PhoneBookSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('phone_books')->insert([
            [
            'name' => 'julio',
            'address' => 'dennenrodepad 897',
            'phone' => 686118887,
            'avatar' => null
        ],
        [
                'name' => 'joel',
                'address' => 'bijlmerpark 897',
                'phone' => 65878421,
                'avatar' => null
        ]
    ]);
    }
}
