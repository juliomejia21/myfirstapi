<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PhoneBook extends Model
{
    protected $table= 'phone_books';

    protected $fillable= [
      'name',
      'address',
      'phone',
      'avatar'
    ];
    protected $hidden= [
        'created_at', 'updated_at'
    ];
}
