<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    //POST Create User
    public function store(Request $request)
    {
        $input = $request->all();
        $input['password'] = Hash::make($request->password);
        User::create($input);
        return response()->json([
            'res' => true,
            'message' => 'User created'
        ], 200);
    }

    //POST Login
    public function login(Request $request)
    {
        $user = User::whereEmail($request->email)->first();
        if (!is_null($user) && Hash::check($request->password, $user->password)) {
            $token = $user->createToken('phonebook')->accessToken;
            return response()->json([
                'res' => true,
                'token' => $token,
                'message' => 'Welcome' . ' ' . $user->name
            ], 200);
        } else {
            return response()->json([
                'res' => false,
                'message' => 'Account or password wrong. Try again pls'
            ], 200);
        }
    }

    //POST Logout
    public function logout()
    {
        $user = auth()->User();
        $user->tokens->each(function ($token, $key) {
            $token->delete();
        });
        $user->save();

        return response()->json([
            'res' => false,
            'message' => 'Bye Bye'
        ], 200);
    }
}
