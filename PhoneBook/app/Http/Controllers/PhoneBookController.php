<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreatePhoneBookRequest;
use App\Http\Requests\UpdatePhoneBookRequest;
use App\PhoneBook;
use Illuminate\Http\Request;

class PhoneBookController extends Controller
{
    //Get data
    public function index(Request $request)
    {
        if ($request->has('search')){
            $phoneBook = PhoneBook::where('name', 'like', '%'. $request->search.'%')
                ->orWhere('phone', $request->search)
                ->get();
        }else{
            $phoneBook=PhoneBook::all();
        }
        return $phoneBook;
    }
    //upload avatar
    Private function uploadAvatar($file){
        $nameFile = time() . "." . $file->getClientOriginalExtension();
        $file->move(public_path('pics'), $nameFile);
        return $nameFile;
    }

   //POST insert data
    public function store(CreatePhoneBookRequest $request)
    {
        $input= $request->all();
        if($request->has('pic'))
            $input['pic'] = $this->uploadAvatar($request->pic);

        PhoneBook::create($input);
        return response()->json([
           'res' => true,
            'message' => 'Register was created'
        ], 200);
    }

   //Get- data shown
    public function show(PhoneBook $phonebook)
    {
        return $phonebook;
    }

     //Put- update data
    public function update(UpdatePhoneBookRequest $request, PhoneBook $phonebook)
    {
        $input= $request->all();
        if($request->has('photo'))
            $input['photo'] = $this->uploadAvatar($request->photo);

        $phonebook->update($input);
        return response()->json([
            'res' => true,
            'message' => 'Register was updated'
        ], 200);
    }

   //Delete a register
    public function destroy($id)
    {
        PhoneBook::destroy($id);
        return response()->json([
            'res' => true,
            'message' => 'Register was deleted'
        ], 200);
    }
}
